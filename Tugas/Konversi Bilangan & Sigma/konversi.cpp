#include <stdio.h>
#include <iostream>
#include <cmath>
#include <stdlib.h>

using namespace std;

int desimal, oktal, bit, pangkat, sisa, hasil;
long long biner1, biner2, biner3, binnum, rem, quot;
char hexa[100], kembali;

// Fungsi 1
void    desimalBiner (int desimal) {
    
	if (desimal>1) {
    	desimalBiner(desimal/2);
	}

	bit = desimal % 2;
	cout << bit << "";
}

// Fungsi 2
void octalToBiner (int octal) {
	int decimal = 0; 
    int i = 0;
	long long biner = 0;

	while (octal != 0) {
        decimal += (octal%10) * pow(8,i);
        ++i;
        octal/=10;
	}

	i = 1;
	while (decimal != 0) {
        biner += (decimal % 2) * i;
        decimal /= 2;
        i *= 10;
	}

	cout << biner << endl;
}

// Fungsi 3
void hexaToBiner(string hexa) { 
    long int i = 0; 
        while (hexa[i]) { 
            switch (hexa[i]) { 
            case '0': 
            cout << "0000"; 
            break; 
            case '1': 
            cout << "0001"; 
            break; 
            case '2': 
            cout << "0010"; 
            break; 
            case '3': 
            cout << "0011"; 
            break; 
            case '4': 
            cout << "0100"; 
            break; 
            case '5': 
            cout << "0101"; 
            break; 
            case '6': 
            cout << "0110"; 
            break; 
            case '7': 
            cout << "0111"; 
            break; 
            case '8': 
            cout << "1000"; 
            break; 
            case '9': 
            cout << "1001"; 
            break; 
            case 'A': 
            case 'a': 
            cout << "1010"; 
            break; 
            case 'B': 
            case 'b': 
            cout << "1011"; 
            break; 
            case 'C': 
            case 'c': 
            cout << "1100"; 
            break; 
            case 'D': 
            case 'd': 
            cout << "1101"; 
            break; 
            case 'E': 
            case 'e': 
            cout << "1110"; 
            break; 
            case 'F': 
            case 'f': 
            cout << "1111"; 
            break; 
            default: 
            cout << "\nInvalid hexadecimal digit "
            << hexa[i]; 
        } 
        i++; 
    } 
}

// Fungsi 4 Biner to Decimal
void binerToDecimal (long long biner1) {
	hasil=0;
	int pangkat=0;
	while(biner1>=1) {
		sisa=biner1%2;
		hasil+=sisa*pow(2,pangkat);
		biner1=biner1/10;
		pangkat++;
	}
	cout<<hasil<<endl;
}

// Fungsi 5
void binerToOktal(long long biner2) {
	int octal = 0, decimal = 0, i = 0;
	while(biner2 != 0) {
        	decimal += (biner2%10) * pow(2,i);
        	++i;
        	biner2/=10;
	}
	i = 1;
	while (decimal != 0) {
        	octal += (decimal % 8) * i;
        	decimal /= 8;
        	i *= 10;
	}
	cout <<  octal << endl ;
}

// Fungsi 6

void binerToHexa(int biner3){
    int hex[1000];
    int i = 1, j = 0, rem, dec = 0;

while (biner3 > 0) 
  {
   rem = biner3 % 2;
   dec = dec + rem * i;
   i = i * 2;
   biner3 = biner3 / 10;
  }
   i = 0;
  while (dec != 0) 
  {
   hex[i] = dec % 16;
   dec = dec / 16;
   i++;
  }
  for (j = i - 1; j >= 0; j--)
  {
   if (hex[j] > 9) 
   {
    cout<<(char)(hex[j] + 55);
   } 
   else
   {
    cout<<hex[j];
   }
  }
  cout << endl;

}

int main () {
    awal:
    system("clear");
	int pilihan;
	cout << "===========================================================================\n";
	cout << "Konversi Bilangan\n";
	cout << "1. Desimal to Biner\n";
	cout << "2. Oktal to Biner\n";
	cout << "3. Hexa to Biner\n\n";

    cout << "4. Biner to Desimal\n";
    cout << "5. Biner to Oktal\n";
    cout << "6. Biner to Hexa\n";
    cout << "===========================================================================\n\n";
	cout << "Masukkan Pilihan: "; cin >> pilihan;
    cout << "\n";
	
if ( pilihan == 1 ){
    cout << "Masukkan bilangan desimal : "; cin >> desimal;
    cout << "Hasil Biner : "; desimalBiner(desimal); 
} else if ( pilihan == 2 ){
    cout << ""; cin >> oktal;
    cout << "Hasil Biner : "; octalToBiner(oktal); 
} else if ( pilihan == 3 ){
    cout << "Masukkan bilangan Hexa : "; cin>>hexa;
    cout << "Hasil Biner : "; hexaToBiner(hexa);
} else if ( pilihan == 4 ){
    cout << "Masukkan bilangan Biner : "; cin>>biner1;
    cout << "Hasil Desimal : "; binerToDecimal(biner1);
} else if ( pilihan == 5 ){
    cout << "Masukkan bilangan Biner : "; cin>>biner2;
    cout << "Hasil Oktal : "; binerToOktal(biner2);
} else if ( pilihan == 6){
    cout << "Masukkan bilangan Biner : "; cin>>biner3;
    cout << "Hasil Hexa : "; binerToHexa(biner3);
} else {
    cout<<"Pilihan Tidak Ada";
}

cout << endl; cout << endl;
cout << "Ingin melakukan konversi kembali (y/n)? ";
cin >> kembali; cout << "\n" << endl;

if ( kembali == 'y' || kembali == 'Y') {
    goto awal;
} else {
    cout << "Terima kasih telah menggunakan program konversi bilangan\n";
    cout << "===========================================================================\n" << endl;
}

return 0;
}